<br />
<p align="center">
  <a href="https://github.com/Fen0day/Rating-grades-Calculator">
    <img src="icon.ico" alt="Logo" width="80" height="80">
  </a>

  <h1 align="center">Rating/Grades Calculator</h1>

  
    
      

* Program that calculates the arithmetic mean of numbers  
* The icon is taken from the [site](https://iconarchive.com/show/ios7-redesign-icons-by-wineass/Calculator-icon.html)

Task from the net

  <h3 align="center">Made with :heart: by <a href="https://github.com/Toshi2D"> Toshi</h3>


